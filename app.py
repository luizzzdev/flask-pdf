import requests
import base64
from flask import Flask, request, jsonify
from PyPDF2 import PdfFileReader, PdfFileMerger

app = Flask(__name__)



@app.route('/api/documents', methods=['POST'])
def respond():
    
    result = {
      "status": "ok"
    }

    headers = {
      "Authorization": 'Basic bWluaGFwcmVzY3JpY2FvOkR5R3g9bjkrRSUqSnk3QD0='
    }

    body = {
       "isSigned":False, 
      "inSingleFile": True,
      "disableMetadata": True
    }

    metadataKeys = ['2.16.76.1.4.2.2=CRM']

    response = requests.post('https://staging-pdf-workspace.nexodata.com.br/api/prescriptions/6601019/documents', headers=headers, json=body)

    documents = response.json()['documents']

    for document in documents:
      with open('./tmp/document.pdf', 'wb') as file:
        file.write(base64.b64decode(document['pdfBase64']))
    
    for document in documents:
      file_in = open('./tmp/document.pdf', 'rb')
      pdf_reader = PdfFileReader(file_in)

      pdf_merger = PdfFileMerger()
      pdf_merger.append(pdf_reader)

      for metadataKey in metadataKeys:
        [key,value] = metadataKey.split('=')

        pdf_merger.addMetadata({
          f'/{key}': value,
        })

      file_out = open('./tmp/document.pdf', 'wb')
      pdf_merger.write(file_out)

      file_in.close()
      file_out.close()
    

    pdfBase64 = None

    with open('./tmp/document.pdf', 'rb') as file:
      pdfBase64 = base64.b64encode(file.read()).decode('utf-8')

    result['data'] = pdfBase64

    return jsonify(result)


# A welcome message to test our server
@app.route('/')
def index():
    return "<h1>Welcome to our server !!</h1>"

if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    app.run(threaded=True, port=5000, debug=True)